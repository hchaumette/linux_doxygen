###############################################################################
#
# See https://gcc.gnu.org/onlinedocs/gcc/Developer-Options.html#Developer-Options
#
###############################################################################

KBUILD := ../build/
KSRC := $(abspath ../linux/)

# Remove if not cross-compiling
export CROSS_COMPILE := arm-none-eabi-
export ARCH := arm

all: doxygen

.PHONY := src_files defines kernel

src_files.txt: src_files
doxygen_input: src_files.txt
defines.txt: defines

# defines as dependencies as both need a clean build and build_log.txt dirties
# the build directory.
# TODO Add modules
# TODO Consider "KCPPFLAGS += -M -MF <file>"
build_log: defines.txt
	$(MAKE) -C $(KBUILD) --always-make --debug=v zImage | tee build_log.txt

src_files: $(KBUILD)/vmlinux
	# Default gdb works fine, no need for a cross-gdb apparently
	gdb -batch -ex 'info sources' $(KBUILD)/vmlinux \
		| grep --only-matching '\(\/[-_[:alnum:]]\+\)\+\.[chS]' \
		| tee src_files.txt
	echo -n "INPUT += " > doxygen_input
	tr '\n' ' ' < src_files.txt >> doxygen_input

kernelversion:
	$(MAKE) -C $(KBUILD) -s kernelversion | awk '{print "PROJECT_NUMBER += " "\"" $$0 "\""}' > doxygen_kernelversion

# TODO Add modules
# Consider using doxygen's INPUT_FILTER
defines:
	# FIXME The normal output of gcc/cpp commands is replaced by a macro dump, i.e. file.o
	# becomes a define list... Which causes the build to fail.
	# -E and -dM must be used together
	$(MAKE) -C $(KBUILD) --always-make --keep-going KCPPFLAGS="-E -dM" drivers/net/phy/mdio_bus.o | tee defines.txt

kernel: $(KBUILD)/.config
	$(MAKE) -C $(KBUILD) $(MAKECMDGOALS)

clean olddefconfig:
	$(MAKE) -C $(KBUILD) $(MAKECMDGOALS)

%_defconfig:
	$(MAKE) -C $(KSRC) O=$(KBUILD) $(MAKECMDGOALS)

config_macros: $(KBUILD)/.config
	awk 'BEGIN {printf "PREDEFINED +=\t"} /^CONFIG_/ {printf "%s \\\n\t\t", $$0}' $(KBUILD).config > config_macros

doxygen: doxygen_input kernelversion
	echo "STRIP_FROM_PATH += $(KSRC)" > doxygen_strip_path
	echo "STRIP_FROM_INC_PATH += $(KSRC)" >> doxygen_strip_path
	doxygen Doxyfile 2> doxygen_log.txt

# $(CROSS_COMPILE)readelf --string-dump=.debug_str $(KBUILD)/vmlinux | egrep -o '/home/hubert/bitbucket/linux/.*\.[c|h|S]' > readelf.out
# $(CROSS_COMPILE)strings $(KBUILD)/vmlinux | egrep '/home/hubert/bitbucket/linux/.*\.[c|h|S]' > strings.out
