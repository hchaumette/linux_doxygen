This is an attempt to use doxygen to generate a comprehensive and graphical
documentation of the Linux kernel.

TODO link to repository.

The kernel supports 30 architectures, +drivers, modularity, complexity -> impossible and useless to document it all.
=> document only the code used in a particular configuration, extracted directly from kernel image (might be possible to do it from compiler, but I am no kbuild/makefile/gcc wizard).

What am I trying to achieve?

How?

Limitations
-----------

- Some files are ignored (according to `irq.c`, `linux/errno.h` and others for exemple);
- Kerneldoc's syntax is different from doxygen's, so a lot of erroneous parsing (see the...);
- Some macros treated as variables and functions. Some are declared in `predefined_macros` to be ignored.

Structure
---------

Expected directory structure:

	linux/					linux sources
	linux_doxygen/
		Makefile			project makefile
		Doxyfile			doxygen configuration file
		html/index.html			generated documentation entry point
	build/					linux build directory

Build
-----

The makefile is set up for ARM cross compilation. To compile for other architectures, remove or edit `ARCH` and `CROSS_COMPILE` from the makefile and select your own configuration (`x86_64_defconfig` for exemple).

	cd linux_doxygen/
	mkdir ../build/
	make vexpress_defconfig		# select vexpress_defconfig (ARM toolchain needed)
	make kernel			# build kernel with ../build/.config
	make				# generate documentation
	xdg-open html/index.html	# view the documentation
